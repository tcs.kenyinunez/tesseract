package utilitarian;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import utilitarian.Excel;

// Clase que contiene los campos del excel ha ser usado en la clase Test
public class ExcelAccountOpening extends Excel{
	
	// Ubicación del pool de datos
	public static String nombreArchivo = "C:\\Users\\kenxh\\OneDrive\\Escritorio\\Automation\\Automation\\src\\test\\resources\\data\\ExcelAccountOpenning.xlsx";
	
	// Variables de la Cuenta
	private int nroCorrelativo;
	private String campo;

	// Getters and Setters de la cuenta
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}

	// Lectura de cada campo en el excel
	public static ArrayList<ExcelAccountOpening> leerExcel() throws IOException{
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<ExcelAccountOpening> arrayObject = new ArrayList<ExcelAccountOpening>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			ExcelAccountOpening tcParameters = new ExcelAccountOpening();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCampo(formatter.formatCellValue(row.getCell(1)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	
	// Establecer el valor de la celda en el excel
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}