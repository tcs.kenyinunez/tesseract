package tool;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Sikulix {
	public Screen screen = new Screen();
	public Pattern element;
	public String rutaImagen = "";
	public int xInicio = 0;
	public int yInicio = 0;
	public int xFinal = 0;
	public int xMedio = 0;
	public int yMedio = 0;
	public Match m;
	public int longitudCelda = 0;
	public int alturaCelda = 0;

	public Sikulix(String rutaImagen) {
		this.rutaImagen = rutaImagen;
		element = new Pattern(rutaImagen);
	}

	// Evalua si elemento se encuentra en la pantalla
	public boolean exists() {
		if (screen.exists(element.similar(0.5f)) == null) {
			System.out.println("El elemento con ruta : " + this.rutaImagen + " no se encontr�");
			return false;
		}
		return true;
	}

	// Espera por 10 segundos el elemento
	public void wait(String rutaImage) {
		try {
			screen.wait(element, 10);
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha encontrado el elemento " + this.rutaImagen);
		}
	}

	// Click
	public void click() {
		element = new Pattern(rutaImagen);
		if (this.exists()) {
			try {
				this.getCoordsImagen();
				screen.click(element);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// Espera a que el elemento desapareciera
	public void waitInvisibility() throws Exception {
		try {
			int seconds = 0;
			while (this.exists()) {
				this.hover();
				Thread.sleep(1000);
				seconds++;
				if (seconds == 30) {
					throw new Exception(
							"Se espero 30 segundos a que el elemento " + this.rutaImagen + " desapareciera");
				}
			}
			return;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void hover() {
		element = new Pattern(rutaImagen);
		if (this.exists()) {
			this.getCoordsImagen();
			screen.hover();
		}
	}

	// Espera por 30 segundos el elemento
	public void waitAtImage() {
		try {
			int x = 0;
			while (!this.exists()) {
				Thread.sleep(1000);
				x++;
				if (x == 30) {
					throw new FindFailed("");
				}
			}
			this.getCoordsImagen();
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha encontrado el elemento " + this.rutaImagen);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Limpiar la caja de texto
//	public void clearText() throws InterruptedException {
//		this.click();
//		AutoIt auto = new AutoIt();
//		auto.getAutoIt().send("{HOME}{END}{BACKSPACE}", false);
//		Thread.sleep(1000);
//	}

	// Obtener coordenadas de la imagen
	public void getCoordsImagen() {
		try {
			m = screen.find(element);
			xInicio = m.getTopLeft().getX();
			yInicio = m.getTopLeft().getY();
			xFinal = m.getTopRight().getX();
			xMedio = m.getCenter().getX();
			yMedio = m.getCenter().getY();
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Desplazar con el mouse a la izquierda
//	public void resizeElement(int pixelLeft) throws FindFailed {
//		this.getCoordsImagen();
//		AutoIt auto = new AutoIt();
//		auto.getAutoIt().mouseClickDrag("left", xFinal, yMedio, xFinal + pixelLeft, yMedio);
//	}

	// Capturar tama�o del elemento
	public void captureSizeElement() throws InterruptedException, FindFailed, AWTException, IOException {
		this.getCoordsImagen();
		Thread.sleep(1000);
		longitudCelda = (m.getCenter().getX() - m.getTopLeft().getX()) * 2;
		alturaCelda = (m.getCenter().getY() - m.getTopLeft().getY()) * 2;
		int times = 2;
	}

	// Capturar la celda
	public String captureCell(int xInicio, int yInicio, int xFinal, int yFinal, int index)
			throws AWTException, IOException {
		Robot robot = new Robot();
		Rectangle rectanguloFoto = new Rectangle(xInicio, yInicio, xFinal, yFinal);
		System.out.println(rectanguloFoto.toString());
		BufferedImage screenShot = robot.createScreenCapture(rectanguloFoto);
		ImageIO.write(screenShot, "png", new File(".\\Resources\\cell" + index + ".png"));
		return ".\\Resources\\cell" + index + ".png";
	}

	// Clic en la imagen
	public void ClickAImagen(String rutaImagen) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern inicioButton = new Pattern(rutaImagen);

		if (s.exists(inicioButton) != null) {
			s.click(inicioButton);
		}
		Thread.sleep(500);
	}

	// Doble clic en la imagen
	public void DoubleClickAImagen(String rutaImagen) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern inicioButton = new Pattern(rutaImagen);

		if (s.exists(inicioButton) != null) {
			s.doubleClick(inicioButton);
		}
		Thread.sleep(500);
	}
	
	// Doble clic en el elemento
	public void DoubleClick() {
		element = new Pattern(rutaImagen);
		if (this.exists()) {
			try {
				this.getCoordsImagen();
				screen.doubleClick(element);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// Ingresar un texto en la imagen
	public void SendKeysAImagen(String rutaImagen, String strUsuario) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern textfield = new Pattern(rutaImagen);
		System.out.println(s.wait(textfield, 1));
		if (s.exists(textfield) != null) {
			s.click(textfield);
			s.type(strUsuario);
		}
		Thread.sleep(500);
	}

	// Ingresar texto
	public void sendKeys(String keys) throws InterruptedException {
		Thread.sleep(1000);
		this.click();
		Thread.sleep(1000);
		screen.type(keys);
	}
}
