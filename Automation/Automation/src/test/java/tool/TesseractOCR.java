package tool;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class TesseractOCR implements Runnable {
	private static final TesseractOCR Read_File = null;
	public static String strValor;

	
	// Obtener texto de imagen
	public static String getTextImage(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\src\\test\\resources\\tesseractOut\\out";
		String tesseract_install_path = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\"");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strValor = Read_File.read_a_file(output_file + ".txt");
			
			String BlankSpace = "";
			String tildeO = "�";
			String de = " de ";
			String textoLogistica = "Log�stica";
			String textoSample = " (Sample)";
			String textoL = "l";
			String textGlobalMedia = "Global Media";
			String textUsuario = "Usuarios";
			String textProducto = "Lustradora";
			String textProducto2 = "Inka Cola";
			
			/*
			String laCadena= strValor;
			StringBuffer nuevaCadena = new StringBuffer();
			for (int i=0;i<laCadena.length(); i++)
			{
			   if (Character.isUpperCase(laCadena.charAt(i))) {
			      nuevaCadena.append(" "+Character.toUpperCase(laCadena.charAt(i)));
			   } else {
			      nuevaCadena.append(laCadena.charAt(i));
			   }
			}*/
			
			
			strValor=strValor.replace(" ", BlankSpace);
			strValor=strValor.replace("é", tildeO);
			strValor=strValor.replace("de", de);
			strValor=strValor.replace("Logistica", textoLogistica);
			strValor=strValor.replace("(Sample)", textoSample);
			strValor=strValor.replace("|", textoL);
			strValor=strValor.replace("GlobalMedia", textGlobalMedia);
			strValor=strValor.replace("Usuario", textUsuario);
			strValor=strValor.replace("Luslradota", textProducto);
			strValor=strValor.replace("InkaCola", textProducto2);
			//strValor=strValor.replace(laCadena, nuevaCadena);
			
			System.out.println("Esta es el valor capturado: " + strValor);
			return strValor;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	// Obtener solo n�mero de imagen
	public static String getTextImageOnlyNumber(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\src\\test\\resources\\tesseractOut\\out";
		String tesseract_install_path = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\" -psm 6");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strValor = Read_File.read_a_file(output_file + ".txt");
			
			String numCero = "0";
			String BlankSpace = "";
			strValor=strValor.replace("O", numCero);
			strValor=strValor.replace(" ", BlankSpace);
			
			System.out.println("Esta es el valor capturado: " + strValor);
			return strValor;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	

	// Obtener solo letras de imagen
	public static String getTextImageOnlyLetters(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\src\\test\\resources\\tesseractOut\\out";
		String tesseract_install_path = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\" nobatch letters");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strValor = Read_File.read_a_file(output_file + ".txt");
			
			String letraO = "O";
			String BlankSpace = "";
			strValor=strValor.replace("0", letraO);
			strValor=strValor.replace(" ", BlankSpace);
			
			System.out.println("Esta es el valor capturado: " + strValor);
			return strValor;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	
	// Leer el archivo
	public static String read_a_file(String file_name) {
		BufferedReader br = null;
		String read_string = "";
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file_name));
			while ((sCurrentLine = br.readLine()) != null) {
				read_string = read_string + sCurrentLine;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return read_string;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
