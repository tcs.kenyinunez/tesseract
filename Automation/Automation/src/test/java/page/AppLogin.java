package page;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class AppLogin {

	WebDriver driver;

	public AppLogin(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebDriver login(String url) throws InterruptedException, AWTException{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src/test/resources/drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		return null;
	}
	
}
