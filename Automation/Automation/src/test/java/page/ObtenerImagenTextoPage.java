package page;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.Logger;

public class ObtenerImagenTextoPage {

	//Anterior
	//public static WebDriver driver = null;
	
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	
	// INSTANCIA DE SELENIUM
	public ObtenerImagenTextoPage(WebDriver driver, Logger log, FileWriter writeEFile){
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	// PASOS DE PRUEBAS
	
	// Ampliar imagen
		// Ancho máximo
	    public static int MAX_WIDTH=800;
	    // Alto máximo
	    public static int MAX_HEIGHT=800;
	
	public static void copyImage(String filePath, String copyPath) {
        BufferedImage bimage = loadImage(filePath);
        if(bimage.getHeight()>bimage.getWidth()){
            int heigt = (bimage.getHeight() * MAX_WIDTH) / bimage.getWidth();
            bimage = resize(bimage, MAX_WIDTH, heigt);
            int width = (bimage.getWidth() * MAX_HEIGHT) / bimage.getHeight();
            bimage = resize(bimage, width, MAX_HEIGHT);
        }else{
            int width = (bimage.getWidth() * MAX_HEIGHT) / bimage.getHeight();
            bimage = resize(bimage, width, MAX_HEIGHT);
            int heigt = (bimage.getHeight() * MAX_WIDTH) / bimage.getWidth();
            bimage = resize(bimage, MAX_WIDTH, heigt);
        }
        saveImage(bimage, copyPath);
    }
	
	// Este método se utiliza para cargar la imagen de disco
    public static BufferedImage loadImage(String pathName) {
        BufferedImage bimage = null;
        try {
            bimage = ImageIO.read(new File(pathName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bimage;
    }
 
    // Este método se utiliza para almacenar la imagen en disco
    public static void saveImage(BufferedImage bufferedImage, String pathName) {
        try {
            String format = (pathName.endsWith(".png")) ? "png" : "jpg";
            File file =new File(pathName);
            file.getParentFile().mkdirs();
            ImageIO.write(bufferedImage, format, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
 // Este método se utiliza para redimensionar la imagen
    public static BufferedImage resize(BufferedImage bufferedImage, int newW, int newH) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(newW, newH, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return bufim;
    }
	
	public void IngresarCampo(String Campo) throws InterruptedException{
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys(Campo);
		Thread.sleep(1000);
	}
	
	public void DarBoton() throws Throwable{
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[2]/div[2]/div[2]/center/input[1]")).click();
		Thread.sleep(1000);
	}
	
	public int ValidarCampo(String csv) throws Throwable{
		textVal = driver.findElement(By.xpath("//*[@id=\"hdtb-tls\"]")).getText();
		return ReportePage.verifyField(log,writeEFile,"Campo2",csv,textVal);
	}
	
	public static String dateTime(int format){
		String fechaTiempo = "";
		if(format==0){
			fechaTiempo = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		}else if(format==1){
			fechaTiempo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		}else{
			System.setProperty("current.date.time",new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss").format(new Date()));
		}
		return fechaTiempo;
	}
	
	public static void crearArchivo(String rutaReporte, String texto){
		
		String carpeta = rutaReporte;
		File crear_carpeta = new File(carpeta);
		
		if(crear_carpeta.isDirectory()){
			
			
		}else{
			crear_carpeta.mkdirs();
		}
		
		FileWriter flwriter = null;

		
		try{
			flwriter = new FileWriter(carpeta + "\\ReporteResumenTest.txt");
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
		
			bfwriter.write("Numero de Cuenta"+ "\r\n");
			bfwriter.write(texto + "\r\n");
			bfwriter.write("Fin del reporte");
			bfwriter.close();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			if(flwriter != null){
				try {
					flwriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
}
