package page;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.Logger;

public class AccountOpeningPage {

	//Anterior
	//public static WebDriver driver = null;
	
	WebDriver driver;
	Logger log;
	FileWriter writeEFile;
	String textVal="";
	
	
	// INSTANCIA DE SELENIUM
	public AccountOpeningPage(WebDriver driver, Logger log, FileWriter writeEFile){
		this.driver = driver;
		this.log=log;
		this.writeEFile=writeEFile;
	}
	
	// PASOS DE PRUEBAS
	
	public void IngresarCampo(String Campo) throws InterruptedException{
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys(Campo);
		Thread.sleep(1000);
	}
	
	public void DarBoton() throws Throwable{
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[2]/div[2]/div[2]/center/input[1]")).click();
		Thread.sleep(1000);
	}
	
	public int ValidarCampo(String csv) throws Throwable{
		textVal = driver.findElement(By.xpath("//*[@id=\"hdtb-tls\"]")).getText();
		return ReportePage.verifyField(log,writeEFile,"Campo2",csv,textVal);
	}
	
	public static String dateTime(int format){
		String fechaTiempo = "";
		if(format==0){
			fechaTiempo = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		}else if(format==1){
			fechaTiempo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		}else{
			System.setProperty("current.date.time",new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss").format(new Date()));
		}
		return fechaTiempo;
	}
	
	public static void crearArchivo(String rutaReporte, String texto){
		
		String carpeta = rutaReporte;
		File crear_carpeta = new File(carpeta);
		
		if(crear_carpeta.isDirectory()){
			
			
		}else{
			crear_carpeta.mkdirs();
		}
		
		FileWriter flwriter = null;

		
		try{
			flwriter = new FileWriter(carpeta + "\\ReporteResumenTest.txt");
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
		
			bfwriter.write("Numero de Cuenta"+ "\r\n");
			bfwriter.write(texto + "\r\n");
			bfwriter.write("Fin del reporte");
			bfwriter.close();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			if(flwriter != null){
				try {
					flwriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
}
