package test;

import java.awt.AWTException;
import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Iterator;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

import utilitarian.ExcelAccountOpening;
import utilitarian.ReportUtilities;

import page.AccountOpeningPage;
import page.ReportePage;
import page.ObtenerImagenTextoPage;
import tool.Sikulix;
import tool.TesseractOCR;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import page.AppLogin;

public class ObtenerTextoImagenTest {
    // Escritorio RAIZ
	// Intancias generales
	//private static WebDriver driver = null;
	WebDriver driver;
	File fil;
//  File eFil;
    FileWriter writeFile;
//  FileWriter writeEFile;
    Logger log;
    Properties prop;
//  boolean loginFlag =true;
//  String stTime = ReportUtilities.dateTime(1);
//  int passCount=0;
//  int failCount=0;
//  int iteration=1;
//  String status;
//	String errFileName;
//  ReportePage reportePage;
    ObtenerImagenTextoPage cl;
    
//  private String nombreProceso;
//  private String nombreClaseTest;
//  AppLogin signIn;
//  int migData=0;
//  AccountOpeningPage cl;
    
    private TesseractOCR tesseractOCR = new TesseractOCR();
    
    
   
//  String rutaReporte = "C:\\Users\\kenxh\\OneDrive\\Escritorio\\Reporte Resumen";
    
//  public String getNombreProceso(){
//    return this.nombreProceso = "REPORTE TEST";
//  }
//    
//  public String getnombreClaseTest(){
//    return this.nombreProceso = "ReporteTest";
//  }
    
	
	// Ejecuta antes del @Test
	@BeforeClass
	public void initializeTest() throws Throwable{
		log = LogManager.getLogger(ObtenerTextoImagenTest.class);
		fil = new File(System.getProperty("user.dir")+"/reportes/Report_"+ReportUtilities.dateTime(0)+".html");
		writeFile = new FileWriter(fil);
		Thread.sleep(1000);
		prop = new Properties();
		prop.load(new FileInputStream(System.getProperty("user.dir")+"//src/main/resources/Config.properties"));
		//reportePage.crearReportHeader(writeFile);
	}
	
	@BeforeTest
	public void setUpTest() throws InterruptedException {
	System.setProperty("webdriver.chrome.driver",
			 "C://Users//kenxh//OneDrive//Escritorio//Automation//Automation//src//test//resources//drivers//chromedriver.exe");
	        driver = new ChromeDriver();
	        driver.manage().window().maximize();
	        driver.get("https://www.google.com");
	}
	
	
	@BeforeMethod 
	public WebDriver startTest() throws IOException, InterruptedException, AWTException {
//		errFileName = System.getProperty("user.dir")+"/reportes/file_"+ReportUtilities.dateTime(0)+".txt";
//		eFil = new File(errFileName);
//		writeEFile = new FileWriter(eFil);
		
//		if(loginFlag) {
//			signIn = new AppLogin(driver);
//			driver = signIn.login(prop.getProperty("url"));
//			loginFlag=false;
//			return driver;
//		}
		
//		System.setProperty("webdriver.chrome.driver",
//		 "C://Users//kenxh//OneDrive//Escritorio//Automation//Automation//src//test//resources//drivers//chromedriver.exe");
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.get("https://www.google.com");
		
		
		return null;
		
//		System.setProperty("webdriver.chrome.driver",
//				 "C://Users//kenxh//OneDrive//Escritorio//Automation//Automation//src//test//resources//drivers//chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://www.google.com");
//		
//		
//		return null;
	}
	
//	@BeforeTest
//	public void setUpTest() throws InterruptedException, IOException {
//		System.setProperty("webdriver.chrome.driver",
//				 "C://Users//kenxh//OneDrive//Escritorio//Automation//Automation//src//test//resources//drivers//chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.get("https://www.google.com");
//	}

	// Ejecuta la prueba con conjuntos de datos
//	@DataProvider(name = "Input")
//	public Object[][] dp() throws IOException {
//		Object[][] arrayObject = null;
//		ArrayList<ExcelAccountOpening> arrayExcel = null;
//		arrayExcel = ExcelAccountOpening.leerExcel();
//		arrayObject = new Object[arrayExcel.size()][2];
//		int i = 0;
//		for (ExcelAccountOpening caso : arrayExcel) {
//			arrayObject[i][0] = String.valueOf(i);
//			arrayObject[i][1] = caso;
//			i++;
//		}
//		return arrayObject;
//	}
	

	// Ejecuta la operacin requerida con @Test
	@Test(dataProvider = "testData")
	public void testCase(String Campo, String Campo2) throws Throwable {
		
		try {
				//WebDriverWait w = new WebDriverWait(driver,30);
				//log.warn("Iteration "+iteration + " begins ********************************** ");
				//ReportePage.writeLogHeader(log, writeEFile, iteration);
				//cl = new AccountOpeningPage(driver, log, writeEFile);
				//reportePage = new ReportePage();
				
				//cl.IngresarCampo(Campo);
				
				Sikulix lblCuenta = new Sikulix("\\src\\test\\resources\\img\\WebTest\\textoGoogle.png");
				
				// Ampliar imagen para reconocer texto
				String ruta = lblCuenta.captureCell(lblCuenta.xInicio + 959,
						lblCuenta.yInicio + (1 * lblCuenta.alturaCelda) + 574,
						lblCuenta.longitudCelda + (lblCuenta.alturaCelda + 200), lblCuenta.alturaCelda + 50,1);
						
				ObtenerImagenTextoPage.copyImage(ruta, ruta.substring(0, ruta.length() - 4) + "_copia.png");
				String celda1 = TesseractOCR.getTextImage(ruta.substring(0, ruta.length() - 4) + "_copia.png");
				System.out.println("Texto Obtenido: " + celda1);
				
				Thread.sleep(1000);
				
//				accountOpeningPage = new AccountOpeningPage(driver, log, writeEFile);
//				accountOpeningPage.IngresarCampo(caso.getCampo());
				

//              PARA GENERAR ARCHIVO DE TXT				
//				File archivo;
//				FileWriter escribir;
//				PrintWriter linea;
//				String texto = "";
//				archivo = new File("C:\\Users\\kenxh\\OneDrive\\Escritorio\\Automation\\Automation\\src\\test\\java\\test\\ReporteResumenTest.txt");
//
//				if(!archivo.exists()){
//					try {
//						archivo.createNewFile();
//						
//						Thread.sleep(1000);
//						texto = driver.findElement(By.xpath("//*[@id=\"hdtb-tls\"]")).getText();
//						escribir = new FileWriter(archivo,true);
//						linea = new PrintWriter(escribir);
//						linea.println("Numeo de cuenta");
//						linea.println(texto);
//						linea.close();
//						escribir.close();
//					} catch (Exception e) {
//						System.out.println("Ocurrio un error al crear el archivo txt");
//					}
//				}else{
//					try {
//						Thread.sleep(1000);
//						texto = driver.findElement(By.xpath("//*[@id=\"hdtb-tls\"]")).getText();
//						escribir = new FileWriter(archivo,true);
//						linea = new PrintWriter(escribir);
//						linea.println(texto);
//						linea.close();
//						escribir.close();
//					} catch (Exception e) {
//						System.out.println("Ocurrio un error al editar el archivo txt");
//					}
//				}
				
				//Texto = driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/div[2]/div[2]/div[1]/div[2]/div/a/div/div[2]/div[2]/span/span")).getText();
													
				
				//reportePage.crearReportSummary(writeFile, stTime, ReportUtilities.dateTime(1), passCount, failCount, migData);
				
				
				
				
//				if(fieldCount==0) {
//				  passCount++;	
//				  //ReportePage.crearReportHeaderDetail(writeEFile, stTime, ReportUtilities.dateTime(1), passCount, failCount, migData); 
//				  ReportePage.crearReportDetail(writeFile, iteration, Campo, "PASS", errFileName, "NA");
//				 // POR MIENTRAS ReportePage.crearArchivo_html(writeFile, iteration, Campo, "PASS", errFileName, "NA",rutaReporte, passCount, failCount, stTime,ReportUtilities.dateTime(1),migData);
//				 // ReportePage.crearDetalle_html(writeFile, iteration, Campo, "PASS", errFileName, "NA");	
//				}else {
//				  failCount++;
//				  ReportePage.crearReportDetail(writeFile, iteration, Campo, "FAIL", errFileName, ReportePage.takeSnapShot(driver));
//				}
//				
//				//ReportePage.closeFile(writeEFile, eFil);
//				
//				Thread.sleep(500);
//				driver.findElement(By.xpath("//*[@id=\"logo\"]/img")).click();
//				Thread.sleep(500);
				
				
//				}else {
//					failCount++;
//					
//					Thread.sleep(500);
//					driver.findElement(By.xpath("//*[@id=\"logo\"]/img")).click();
//					Thread.sleep(500);
//					
//					ReportePage.crearArchivo_html(writeFile, iteration, Campo, "FAIL", errFileName, ReportePage.takeSnapShot(driver), rutaReporte, passCount, failCount, stTime,ReportUtilities.dateTime(1));
//				}
//				
			
//				migData++;
			
		} catch (Exception e) {
				
		}
	}
	
	@DataProvider(name="testData")
	public Iterator<Object[]> testData() throws IOException{
		return ReportePage.parseCSV(System.getProperty("user.dir")+"/InputData/ExcelAccountOpenning.csv");
	}
	
	// Despus de ejecutar @Test
	@AfterClass
	public void afterClass() throws Throwable {	
//		ReportePage.crearReportFooter(writeFile, stTime, ReportUtilities.dateTime(1), passCount, failCount, migData);
//		//Desktop.getDesktop().browse(new URI("file:///C:/Users/kenxh/OneDrive/Escritorio/Reporte%20Resumen/Reporte%20Resumen.html"));
//		writeFile.close();
		driver.close();
	}
}